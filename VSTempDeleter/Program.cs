﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;
using VSTempDeleter.Utils;

namespace VSTempDeleter
{
    class Program
    {
        static void Main(string[] args)
        {
            string thisPath = Directory.GetCurrentDirectory();

            var parameterParser = new ConsoleParameterParser(args);

            if (!parameterParser.ArgumentsPassed)
            {
                Console.WriteLine("Путь не передан! \r\nТекущий путь: " + thisPath);

                if (ConsoleParameterParser.QuestionYN(question: "Использовать текущий путь?",
                    printWhenPositiveAsnwer: null,
                    printWhenNegativeAnswer: "Операция отменена."))
                {
                    Console.ReadKey();
                    return;
                }
            }
            else
            {
                if (parameterParser.ParameterPassed("/i"))
                {
                    new Installer().Install();
                    Console.Write("Installation completed.");
                    return;
                }
                if (parameterParser.ParameterPassed("/u"))
                {
                    new Installer().Uninstall();
                    Console.Write("Uninstallation completed.");
                    return;
                }
            }

            thisPath = args[0];

            var clearProcessor = new ClearProcessor();
            clearProcessor.Clear(thisPath);

            Console.ReadKey();
        }
    }
}
