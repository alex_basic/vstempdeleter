﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace VSTempDeleter.Utils
{
    public class Installer
    {
        private void AddToContextMenu(string execAccemblyPath)
        {
            RegistryKey rk = Registry.CurrentUser;
            var shell = rk.OpenSubKey("Software\\Classes\\Directory\\Background\\shell");
            var names = shell.GetSubKeyNames();
            var menuItemCreated = names.Any(x => x.Equals("ClearVStudioBuilds"));
            shell.Close();
            if (menuItemCreated) return;

            RegistryKey sk1 = rk.CreateSubKey("Software\\Classes\\Directory\\Background\\shell\\ClearVStudioBuilds", RegistryKeyPermissionCheck.ReadWriteSubTree);

            sk1.SetValue("MUIVerb", "--=> Очистить bin и obj <=--");
            sk1.SetValue("Position", "Bottom");

            sk1.Close();


            RegistryKey rk2 = Registry.CurrentUser;
            RegistryKey sk2 = rk2.CreateSubKey("Software\\Classes\\Directory\\Background\\shell\\ClearVStudioBuilds\\Command", RegistryKeyPermissionCheck.ReadWriteSubTree);


            sk2.SetValue(null, string.Format("cmd /K \"{0} %v\"", execAccemblyPath.Replace("/", "\\")));

            sk2.Close();
        }

        private void RemoveFromContextMenu()
        {
            RegistryKey rk = Registry.CurrentUser;
            var shell = rk.OpenSubKey("Software\\Classes\\Directory\\Background\\shell");
            var names = shell.GetSubKeyNames();
            var menuItemCreated = names.Any(x => x.Equals("ClearVStudioBuilds"));
            shell.Close();
            if (!menuItemCreated) return;

            RegistryKey rk2 = Registry.CurrentUser;
            var shell2 = rk.OpenSubKey("Software\\Classes\\Directory\\Background\\shell", RegistryKeyPermissionCheck.ReadWriteSubTree);
            shell2.DeleteSubKeyTree("ClearVStudioBuilds");
            shell2.Close();
        }

        public void Install()
        {
            AddToContextMenu(System.Reflection.Assembly.GetExecutingAssembly().Location);
        }

        public void Uninstall()
        {
            RemoveFromContextMenu();
        }
    }
}
