﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSTempDeleter.Utils
{
    public class ConsoleParameterParser
    {
        private string[] _args;
        StringComparison _stringComparison;

        public bool ArgumentsPassed
        {
            get
            {
                return _args != null && _args.Length != 0;
            }
        }

        public ConsoleParameterParser(string[] args, 
            StringComparison stringComparison = StringComparison.InvariantCultureIgnoreCase)
        {
            _args = args;
            _stringComparison = stringComparison;
        }

        //private string GetParameterValue(string parameterName)
        //{
        //    return null;
        //}

        public bool ParameterPassed(string parameterName)
        {
            return _args.Any(x => x.Equals(parameterName, _stringComparison));
        }

        public static bool QuestionYN(string question, string printWhenPositiveAsnwer = null, string printWhenNegativeAnswer = null)
        {
            Console.Write("{0} (y/n): ", question);
            string userAnswer = Console.ReadLine();
            if (!userAnswer.Equals("y"))
            {
                if (printWhenNegativeAnswer != null) Console.WriteLine(printWhenNegativeAnswer);
                return false;
            }

            if (printWhenPositiveAsnwer != null) Console.WriteLine(printWhenPositiveAsnwer);
            return true;
        }
    }
}
