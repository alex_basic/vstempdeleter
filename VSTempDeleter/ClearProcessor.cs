﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSTempDeleter
{
    public class ClearProcessor
    {
        public void Clear(string thisPath) 
        {
            string[] filePathsBin = Directory.GetDirectories(thisPath, "bin", SearchOption.AllDirectories);
            string[] filePathsObj = Directory.GetDirectories(thisPath, "obj", SearchOption.AllDirectories);
            string[] filePaths = new string[filePathsBin.Length + filePathsObj.Length];

            filePathsBin.CopyTo(filePaths, 0);
            filePathsObj.CopyTo(filePaths, filePathsBin.Length);

            int totalCount = 0;

            foreach (string filePath in filePaths)
            {
                if (!filePath.Contains(".git"))
                {
                    Console.WriteLine("Удаляю папку " + filePath + "...");

                    try
                    {
                        Directory.Delete(filePath, true);
                        //File.Delete(filePath);
                        totalCount++;

                    }
                    catch (System.IO.IOException)
                    {
                        Console.WriteLine("ОШИБКА: Каталог доступен только для чтения или же каталог является текущим рабочим каталогом приложения.");
                    }
                    catch (System.UnauthorizedAccessException)
                    {
                        Console.WriteLine("ОШИБКА: Данная программа не имеет необходимого разрешения или каталог содержит файл, доступный только для чтения.");
                    }

                }

            }

            if (totalCount == 0)
            {
                Console.WriteLine("Не было удалено ни одного каталога");
            }

            else
            {
                Console.WriteLine("Всего каталогов удалено: {0}.", totalCount);
            }
        }
    }
}
